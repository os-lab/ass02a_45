#include "bits/stdc++.h"
#include "unistd.h"
#include "sys/wait.h"
#include "sys/shm.h"
#define shmId 1025
#define bufSize 5
#define lLimit 2
#define uLimit 100
using namespace std;

void getTime() {
    time_t now = time(0);
    tm *ltm = localtime(&now);
    printf("Time --> %02d: %02d: %02d\n", ltm->tm_hour, ltm->tm_min, ltm->tm_sec);
}

vector<int> getPrimes() {
    vector<int> primes;
    primes.push_back(2);
    for(int i=3; i < 100; i++) {
        bool prime=true;
        for(int j=0; j < primes.size() && primes[j]*primes[j] <= i;j++) {
            if(i % primes[j] == 0) {
                prime=false;
                break;
            }
        }
        if(prime)
            primes.push_back(i);
    }
    return primes;
}

void printShm(int *arr, int size) {
    int iter;
    for (iter = 0; iter < size; iter++)
        printf("%d ", arr[iter]);
    printf("\n");
}

int main() {
    pid_t pid;
    vector <int> primes = getPrimes();
    int np, nc, iter, status, shm, *arr, state = -1, st = 0, fl = 0, pSize = primes.size();
    shm = shmget(shmId, bufSize * sizeof(int), IPC_CREAT | 0666);
    if (shm < 0) {
        printf("Couldn't create shared memory... Gomennasai!\n");
        exit(0);
    }
    arr = (int*) shmat(shm, NULL, 0);
    scanf(" %d %d", &np, &nc);
    for (iter = 0; iter < np + nc; iter ++) {
        pid = fork();
        if (pid == 0) {
            if (iter < np) {
                while (state > 0);
                srand (time(NULL));
                arr[fl] = primes [ rand() % pSize ];
                printf("Producer %2d => %2d    ", iter + 1, arr[fl]);
                getTime();
                // printShm(arr, bufSize);
                fl = (fl + 1) % bufSize;
                if (st == fl) state = 1;
                else state = 0;
                sleep( rand() % 5);
            } else {
                while (state < 0);
                srand (time(NULL));
                printf("Consumer %2d => %2d    ", iter - np + 1, arr[st]);
                arr[st] = 0;
                getTime();
                // printShm(arr, bufSize);
                st = (st + 1) % bufSize;
                if (st == fl) state = -1;
                else state = 0;
                sleep( rand() % 5);
            }
        } else {
            sleep(30);
            kill(pid, SIGKILL);
        }
    }
    shmdt(arr);
    shmctl (shm, IPC_RMID, NULL);
}